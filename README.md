# springboot-thymeleaf-crud-pagination-sorting-webapp

<h2>Library App</h2>

Developed a app using:

 - Spring Boot 
 - Multi Maven Project
 - Thymeleaf 
 - Spring Data JPA
 - Hibernate
 - MySQL DB 
 - Pagination and Sorting
 - User Login / Registration 


---
Thymeleaf

  Thymeleaf is a modern server-side Java template engine for both web and standalone environments, capable of processing HTML, XML, JavaScript, CSS  and even plain text. The main goal of Thymeleaf is to provide an elegant and highly-maintainable way of creating templates.
It's commonly used to generate HTML views for web apps. Thymeleaf template engine is commonly used with the Spring MVC framework to develop web applications.


When do I use JPA?
- SQL Database
- Static Domain Model
- Mostly CRUD
- Mostly Simple Queries/Mappings

Database setup:
By default the app runs on librarydb. If you want to switch to MySQL follow the steps below:
 Uncomment the following variables in frontend/application.properties and change IP, PORT and DB_NAME
      
        MySQL
        # DATASOURCE (DataSourceAutoConfiguration & DataSourceProperties)
        spring.datasource.url=jdbc:mysql://localhost:3306/librarydb?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false
        spring.datasource.username=root
        spring.datasource.password=root
        

 ***

How to run the project:

 - Creating librarydb schema in MySQL Workbench 
 - Run project folders in Eclipse IDE
 - Click the localhost:8080/ url.

 
 ![Screenshot](login.PNG)

 ![Screenshot](home.PNG)

 ![Screenshot](author.PNG)




### Zeynep Köse

