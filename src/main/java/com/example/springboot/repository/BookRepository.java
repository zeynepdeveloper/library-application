package com.example.springboot.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.springboot.model.Book;


@Repository
public interface BookRepository extends JpaRepository<Book, Long>{
	
    @Query(value = "SELECT * FROM books t WHERE " +
            "LOWER(t.name) LIKE LOWER(CONCAT('%',:searchTerm, '%')) OR " +
            "LOWER(t.serial_name) LIKE LOWER(CONCAT('%',:searchTerm, '%')) OR"+
            "LOWER(t.authot_name) LIKE LOWER(CONCAT('%',:searchTerm, '%')) OR"+
            "LOWER(t.isbn) LIKE LOWER(CONCAT('%',:searchTerm, '%'))",
            nativeQuery = true
    )
    List<Book> findBySearchTermNative(@Param("searchTerm") String searchTerm);

}
