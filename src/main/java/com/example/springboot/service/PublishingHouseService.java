package com.example.springboot.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.example.springboot.model.PublishingHouse;

public interface PublishingHouseService {
	List<PublishingHouse> getAllPublishingHouses();
	void savePublishingHouse(PublishingHouse publishingHouse);
	PublishingHouse getPublishingHouseById(long id);
	void deletePublishingHouseById(long id);
	Page<PublishingHouse> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
