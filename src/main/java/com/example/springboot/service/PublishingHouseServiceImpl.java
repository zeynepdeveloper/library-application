package com.example.springboot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.springboot.model.PublishingHouse;
import com.example.springboot.repository.PublishingHouseRepository;

@Service
public class PublishingHouseServiceImpl implements PublishingHouseService {

	@Autowired
	private PublishingHouseRepository publishingHouseRepository;

	@Override
	public List<PublishingHouse> getAllPublishingHouses() {
		return publishingHouseRepository.findAll();
	}

	@Override
	public void savePublishingHouse(PublishingHouse publishingHouse) {
		this.publishingHouseRepository.save(publishingHouse);
	}

	@Override
	public PublishingHouse getPublishingHouseById(long id) {
		Optional<PublishingHouse> optional = publishingHouseRepository.findById(id);
		PublishingHouse publishingHouse = null;
		if (optional.isPresent()) {
			publishingHouse = optional.get();
		} else {
			throw new RuntimeException(" PublishingHouse not found for id :: " + id);
		}
		return publishingHouse;
	}

	@Override
	public void deletePublishingHouseById(long id) {
		this.publishingHouseRepository.deleteById(id);
	}

	@Override
	public Page<PublishingHouse> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
			Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		return this.publishingHouseRepository.findAll(pageable);
	}
}
