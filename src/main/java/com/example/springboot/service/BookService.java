package com.example.springboot.service;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;

import com.example.springboot.model.Book;

public interface BookService {
	List<Book> getAllBooks();
	void saveBook(Book book);
	Book getBookById(long id);
	void deleteBookById(long id);
	Page<Book> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
	
}
 