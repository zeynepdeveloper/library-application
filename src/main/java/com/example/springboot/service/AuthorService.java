package com.example.springboot.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.example.springboot.model.Author;

public interface AuthorService {
	List<Author> getAllAuthors();
	void saveAuthor(Author author);
	Author getAuthorById(long id);
	void deleteAuthorById(long id);
	Page<Author> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
