package com.example.springboot.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.springboot.model.PublishingHouse;
import com.example.springboot.service.PublishingHouseService;

@Controller
public class PublishingHouseController {

	@Autowired
	private PublishingHouseService publishingHouseService;
	
	// display list of publishingHouses
	@GetMapping("/publishingHousehome")
	public String viewHomePage(Model model) {
		return findPaginated(1, "publishingHouseName", "asc", model);		
	}
	
	@GetMapping("/showNewPublishingHouseForm")
	public String showNewPublishingHouseForm(Model model) {
		// create model attribute to bind form data
		PublishingHouse publishingHouse = new PublishingHouse();
		model.addAttribute("publishingHouse", publishingHouse);
		return "new_publishingHouse";
	}
	
	@PostMapping("/savePublishingHouse")
	public String savePublishingHouse(@ModelAttribute("publishingHouse") PublishingHouse publishingHouse) {
		// save publishingHouse to database
		publishingHouseService.savePublishingHouse(publishingHouse);
		return "redirect:/";
	}
	
	@GetMapping("/showFormForUpdate/{id}")
	public String showFormForUpdate(@PathVariable ( value = "id") long id, Model model) {
		
		// get publishingHouse from the service
		PublishingHouse publishingHouse = publishingHouseService.getPublishingHouseById(id);
		
		// set publishingHouse as a model attribute to pre-populate the form
		model.addAttribute("publishingHouse", publishingHouse);
		return "update_publishingHouse";
	}
	
	@GetMapping("/deletePublishingHouse/{id}")
	public String deletePublishingHouse(@PathVariable (value = "id") long id) {
		
		// call delete publishingHouse method 
		this.publishingHouseService.deletePublishingHouseById(id);
		return "redirect:/";
	}
	
	
	@GetMapping("/page/{pageNo}")
	public String findPaginated(@PathVariable (value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			Model model) {
		int pageSize = 5;
		
		Page<PublishingHouse> page = publishingHouseService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<PublishingHouse> listPublishingHouses = page.getContent();
		
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("listPublishingHouses", listPublishingHouses);
		return "publishingHouse";
	}
}
