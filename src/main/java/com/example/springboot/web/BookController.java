package com.example.springboot.web;

import java.util.List;
import java.util.Set;
import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.springboot.model.Book;
import com.example.springboot.repository.BookRepository;
import com.example.springboot.service.BookService;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;
	
	@Autowired
	private BookRepository bookRepository;
	
	// display list of books
	@GetMapping("/bookhome")
	public String viewHomePage(Model model) {
		return findPaginated(1, "name", "asc", model);		
	}
	
	@GetMapping("/showNewBookForm")
	public String showNewBookForm(Model model) {
		// create model attribute to bind form data
		Book book = new Book();
		model.addAttribute("book", book);
		return "new_book";
	}
	
	@PostMapping("/saveBook")
	public String saveBook(@ModelAttribute("book") Book book) {
		// save book to database
		bookService.saveBook(book);
		return "redirect:/";
	}
	
	@GetMapping("/showFormForUpdateBook/{id}")
	public String showFormForUpdate(@PathVariable ( value = "id") long id, Model model) {
		
		// get book from the service
		Book book = bookService.getBookById(id);
		
		// set book as a model attribute to pre-populate the form
		model.addAttribute("book", book);
		return "update_book";
	}
	
	@GetMapping("/deleteBook/{id}")
	public String deleteBook(@PathVariable (value = "id") long id) {
		
		// call delete book method 
		this.bookService.deleteBookById(id);
		return "redirect:/";
	}
	
	
	@GetMapping("/pageBook/{pageNo}")
	public String findPaginated(@PathVariable (value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			Model model) {
		int pageSize = 5;
		
		Page<Book> page = bookService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<Book> listBooks = page.getContent();
		
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("listBooks", listBooks);
		return "book";
	}
	

    
    
	@GetMapping("/searchBookTerm")
	public String search(@PathVariable (value = "search") String term) {
			
		this.bookRepository.findBySearchTermNative(term);

		return "book";
	}

}
