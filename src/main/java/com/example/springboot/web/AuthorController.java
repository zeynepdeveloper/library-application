package com.example.springboot.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.springboot.model.Author;
import com.example.springboot.service.AuthorService;

@Controller
public class AuthorController {

	@Autowired
	private AuthorService authorService;
	
	// display list of authors
	@GetMapping("/authorhome")
	public String viewHomePage(Model model) {
		return findPaginated(1, "authorName", "asc", model);		
	}
	
	@GetMapping("/showNewAuthorForm")
	public String showNewAuthorForm(Model model) {
		// create model attribute to bind form data
		Author author = new Author();
		model.addAttribute("author", author);
		return "new_author";
	}
	
	@PostMapping("/saveAuthor")
	public String saveAuthor(@ModelAttribute("author") Author author) {
		// save author to database
		authorService.saveAuthor(author);
		return "redirect:/";
	}
	
	@GetMapping("/showFormForUpdateAuthor/{id}")
	public String showFormForUpdate(@PathVariable ( value = "id") long id, Model model) {
		
		// get author from the service
		Author author = authorService.getAuthorById(id);
		
		// set author as a model attribute to pre-populate the form
		model.addAttribute("author", author);
		return "update_author";
	}
	
	@GetMapping("/deleteAuthor/{id}")
	public String deleteAuthor(@PathVariable (value = "id") long id) {
		
		// call delete author method 
		this.authorService.deleteAuthorById(id);
		return "redirect:/";
	}
	
	
	@GetMapping("/pageAuthor/{pageNo}")
	public String findPaginated(@PathVariable (value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			Model model) {
		int pageSize = 5;
		
		Page<Author> page = authorService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<Author> listAuthors = page.getContent();
		
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("listAuthors", listAuthors);
		return "author";
	}
}
